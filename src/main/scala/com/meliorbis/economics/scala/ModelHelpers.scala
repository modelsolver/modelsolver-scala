/**
 *
 */
package com.meliorbis.economics.scala

import com.meliorbis.economics.model.State
import com.meliorbis.economics.infrastructure.notifications.ArrayObserver

import com.meliorbis.numerics.scala.DoubleArray._
import scala.language.implicitConversions

/**
 * Implicit conversions and other little helpers for working with models in Scala
 * 
 * @author Tobias Grasl
 */
object ModelHelpers {

  /**
   * Converts a closure accepting an two double arrays and a state to an IArrayChanged
   * callback
   */
  implicit def arrayChangedConv[S <: State[_]](fn: (DoubleArray, DoubleArray, S  ) => Unit) = {
    new ArrayObserver[S] {
      def changed(oldVal: com.meliorbis.numerics.generic.primitives.DoubleArray[_], newVal: com.meliorbis.numerics.generic.primitives.DoubleArray[_], state: S ) = fn(oldVal, newVal, state)
    }
  }
}